import React from 'react';
import "./CountryList.css";

const CountryList = props => {
    return (
        <div className="Countries">
        <button className="CountryList" onClick={props.onClick}>
            {props.countryName}
        </button>
        </div>
    );
};

export default CountryList;