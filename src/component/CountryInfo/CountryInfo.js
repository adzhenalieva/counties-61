import React from 'react';
import './CountryInfo.css';

const CountryInfo = props => {
    return (
        <div className="CountryInfo">
            <h3>{props.name}</h3>
            <img className="Flag" src={props.flag} alt="flag"/>
            <p>Capital: {props.capital}</p>
            <p>Population: {props.population}</p>
            <p>Region: {props.region}</p>
            <p>Borders: {props.borders}</p>
            </div>
    );
};

export default CountryInfo;