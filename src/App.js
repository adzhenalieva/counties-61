import React, {Component} from 'react';
import CountryList from "./component/CountryList/CountryList";
import CountryInfo from "./component/CountryInfo/CountryInfo";
import axios from 'axios';
import PerfectScrollbar from 'react-perfect-scrollbar';

import './App.css';


class App extends Component {
    state = {
        countries: [],
        showCountryInfo: false,
        borders: null,
        country: []
    };

    componentDidMount() {


        axios.get('https://restcountries.eu/rest/v2/all').then(response => {
            return response.data;
        }).then(countries => {
            this.setState({countries});
        }).catch(error => {
            console.log(error);
        });
    }

    showInfo = (name) => {

        axios.get(`https://restcountries.eu/rest/v2/name/${name}`).then(response => {
            const borders = response.data[0].borders;
            const promises = borders.map(code => {
                return axios.get('https://restcountries.eu/rest/v2/alpha/' + code);
            });
            Promise.all(promises).then(response => {
                const names = response.map(country => {
                    return country.data.name;
                });
                if (names.length === 0) {
                    this.setState({
                        borders: "There is no border countries"
                    })
                } else if (names.length > 0) {
                    this.setState({
                        borders: names.join(', ')
                    })
                }

            });
            this.setState({
                country: response.data, showCountryInfo: true
            })
        })
    };


    render() {

        return (
            <div className="App">
                    <PerfectScrollbar className="ScrollBar">
                        <div>
                            {this.state.countries.map((country, id) =>
                                <CountryList
                                    key={id}
                                    countryName={country.name}
                                    onClick={() => this.showInfo(country.name)}/>
                            )}
                        </div>
                    </PerfectScrollbar>
                {this.state.showCountryInfo ?
                    <CountryInfo
                        name={this.state.country[0].name}
                        flag={this.state.country[0].flag}
                        capital={this.state.country[0].capital}
                        population={this.state.country[0].population}
                        region={this.state.country[0].region}
                        borders={this.state.borders}
                    />
                    : <p>Choose the country</p>
                }

            </div>
        );
    }
}

export default App;
